import pkg from './package'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  mode: 'universal',

  /*
  ** Headers of the page

  */
  head: {
    title: "Numquam Yesterday Said Nyet",
    script: [
      { src: 'https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js' },
      { src: 'semantic/dist/semantic.min.js' }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'semantic/dist/semantic.css'},
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/frow@3/dist/frow.min.css' }, 
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=News+Cycle|Material+Icons' },
      { rel: 'stylesheet', href: 'https://https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css' },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#607d8b' },
  /*
  ** Customize the base url
  */
  // router: {
  //   base: '/numquam-times-two/'
  // },
  // generate: {
  //   dir: 'public  ',
  // },

  /*
  ** Global CSS
  */
  css: [
    'assets/styles/numquam.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
